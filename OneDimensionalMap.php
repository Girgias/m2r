<?php
declare(strict_types=1);

class OneDimensionalMap
{
    public const N_MAX = 11_000;

    private float $a0;
    private float $a1;
    private float $a2;
    private float $a3;
    private float $a4;
    private float $a5;
    /** CSV of generated coordinates */
    private string $xCoordinates = '';
    private string $yCoordinates = '';
    /** Lyapunov things, look at page 33 from J.C. Sprott */
    private float $lyapunovExponent = 0;
    private float $LyapunovExponentSum = 0.0;
    private int $N = 0;
    private int $NL = 0;

    public function __construct(float $a0, float $a1, float $a2, float $a3 = 0, float $a4 = 0, float $a5 = 0)
    {
        $this->a0 = $a0;
        $this->a1 = $a1;
        $this->a2 = $a2;
        $this->a3 = $a3;
        $this->a4 = $a4;
        $this->a5 = $a5;
    }

    public function generateCoordinates(int $iterations, float $x): void
    {
        for ($i = 0; $i <= $iterations; $i++) {
            $y = $this->iterate($x);
            /* Add coordinates to plot */
            $this->xCoordinates .= $x . ',';
            $this->yCoordinates .= $y . ',';
            /* replace x by y (i.e. x_n+1) */
            $x = $y;
        }

        /* trim most right comma ', ' */
        $this->xCoordinates = rtrim($this->xCoordinates, ',');
        $this->yCoordinates = rtrim($this->yCoordinates, ',');
    }

    public function iterate(float $x): float
    {
        return $this->a0 + $this->a1*$x + $this->a2*$x**2 + $this->a3*$x**3 + $this->a4*$x**4 + $this->a5*$x**5;
    }

    public function mapToPreviousNthPoint(int $previousN): void
    {
        if ($previousN === 1) {
            return;
        }
        // String manipulation
        $nthOccurrenceFromStart = 0;
        $nthOccurrenceFromEnd = 0;
        for ($i = 1; $i < $previousN; $i++) {
            $nthOccurrenceFromEnd = strrpos($this->xCoordinates, ',', $nthOccurrenceFromEnd);
            // +1 because we want to move past the found ','
            $nthOccurrenceFromStart = strpos($this->yCoordinates, ',', $nthOccurrenceFromStart) + 1;
        }
        $this->xCoordinates = substr($this->xCoordinates, 0, $nthOccurrenceFromEnd);
        $this->yCoordinates = substr($this->yCoordinates, $nthOccurrenceFromStart);
    }

    /** Look at page 33 in "Strange Attractors: Creating Patterns in Chaos by Julien C. Sprott" */
    public function calculateLyapunovExponenent(float $x): void
    {
        $df = abs($this->a1 + 2*$this->a2*$x);
        if ($df > 0) {
            $this->LyapunovExponentSum += log($df, 2);
        } else {
            $this->NL++;
        }
        $this->lyapunovExponent = .721347 * $this->LyapunovExponentSum / $this->NL;
    }

    public function getMathMLRepresentation(): string
    {
        return '';
    }

    public function getXCoordinates(): string
    {
        return $this->xCoordinates;
    }

    public function getYCoordinates(): string
    {
        return $this->yCoordinates;
    }
}
