<?php


class TentMap extends OneDimensionalMap
{
    private float $mu;

    public function __construct(float $mu)
    {
        $this->mu = $mu;
    }

    public function iterate(float $x): float
    {
        return $this->mu * min(2*$x, 2*(1-$x));
    }
}
