<?php
declare(strict_types=1);

use MathPHP\LinearAlgebra\Matrix;
use MathPHP\LinearAlgebra\MatrixFactory;

class TwoDimensionalQuadraticMap
{
    public const TRANSIENT_ITERATIONS = 500;
    private const STEP = 0.025;
    /** Coefficients for the quadratic map */
    private float $a0;
    private float $a1;
    private float $a2;
    private float $a3;
    private float $a4;
    private float $a5;
    private float $b0;
    private float $b1;
    private float $b2;
    private float $b3;
    private float $b4;
    private float $b5;
    /** CSV of generated coordinates */
    private string $xCoordinates = '';
    private string $yCoordinates = '';
    /** Matrices for Lyapunov exponent calculation */
    private Matrix $Q;
    /** cumulative addition of diagonal entries of R from QR decomposition */
    private float $diag1 = 0;
    private float $diag2 = 0;
    /** Lyapunov exponents */
    private float $lyapunov1 = 0;
    private float $lyapunov2 = 0;
    /** Lyapunov exponent convergence coordinates */
    private string $lyapunovIteration = '';
    private string $lyapunov1Coordinates = '';
    private string $lyapunov2Coordinates = '';

    /**
     * TwoDimensionalQuadraticMap constructor.
     * $a values correspond to the X_(n+1) coefficients
     * while $b values to correspond to the Y_(n+1) ones.
     */
    public function __construct(
        float $a0, float $a1, float $a2, float $a3, float $a4, float $a5,
        float $b0, float $b1, float $b2, float $b3, float $b4, float $b5
    )
    {
        $this->a0 = $a0;
        $this->a1 = $a1;
        $this->a2 = $a2;
        $this->a3 = $a3;
        $this->a4 = $a4;
        $this->a5 = $a5;
        $this->b0 = $b0;
        $this->b1 = $b1;
        $this->b2 = $b2;
        $this->b3 = $b3;
        $this->b4 = $b4;
        $this->b5 = $b5;
        // Initialize Q and R matrices to identity matrices
        $this->Q = MatrixFactory::identity(2);
    }

    public function generateCoordinates(int $iterations, int $transient,float $x, float $y): void
    {
        if ($iterations < $transient) {
            throw new RuntimeException('Cannot calculate coordinates with less than iterations the transient step');
        }
        // Skip first $TRANSIENT_ITERATIONS iterations for Lyapunov exponent calculation
        for ($i = 0; $i <= $transient; $i++) {
            /* Add coordinates to plot */
            $this->xCoordinates .= $x . ',';
            $this->yCoordinates .= $y . ',';
            [$x, $y] = $this->iterate($x, $y);
        }
        for ($i = 1; $i <= $iterations - $transient; $i++) {
            /* Add coordinates to plot */
            $this->xCoordinates .= $x . ',';
            $this->yCoordinates .= $y . ',';
            [$x, $y] = $this->iterate($x, $y);
        }
        /* final coordinates */
        $this->xCoordinates .= $x;
        $this->yCoordinates .= $y;
    }

    /**
     * This follows Eckmann and Ruelle's algorithm defined in their paper from 1985 and which is explained
     * in Sandri's "Numerical calculation for Lyapunov exponents" page 83
     */
    public function generateCoordinatesWithLyapunovExponents(
        int $iterations,
        int $transient,
        float $x,
        float $y
    ): void {
        if ($iterations < $transient) {
            throw new RuntimeException('Cannot calculate coordinates with less than iterations the transient step');
        }
        // Skip first $TRANSIENT_ITERATIONS iterations for Lyapunov exponent calculation
        for ($i = 0; $i <= $transient; $i++) {
            /* Add coordinates to plot */
            $this->xCoordinates .= $x . ',';
            $this->yCoordinates .= $y . ',';
            [$x, $y] = $this->iterate($x, $y);
        }
        for ($i = 1; $i <= $iterations - $transient; $i++) {
            /* Add coordinates to plot */
            $this->xCoordinates .= $x . ',';
            $this->yCoordinates .= $y . ',';
            $this->calculateLyapunovExponentIteration($x, $y);
            $this->computeLyapunovExponentsFromDiagonal($i);
            $this->lyapunovIteration .= $i . ',';
            $this->lyapunov1Coordinates .= $this->lyapunov1 . ',';
            $this->lyapunov2Coordinates .= $this->lyapunov2 . ',';
            [$x, $y] = $this->iterate($x, $y);
        }
        /* final coordinates */
        $this->xCoordinates .= $x;
        $this->yCoordinates .= $y;
        $this->calculateLyapunovExponentIteration($x, $y);
        $this->computeLyapunovExponentsFromDiagonal($i);
        $this->lyapunovIteration .= $i . ',';
        $this->lyapunov1Coordinates .= $this->lyapunov1 . ',';
        $this->lyapunov2Coordinates .= $this->lyapunov2 . ',';
    }

    public function findBasinAttraction(
        int $iterations,
        float $xNegBoundary,
        float $xPosBoundary,
        float $yNegBoundary,
        float $yPosBoundary
    ): void
    {
        for ($x0 = $xNegBoundary; $x0 <= $xPosBoundary; $x0 += self::STEP) {
            for ($y0 = $yNegBoundary; $y0 <= $yPosBoundary; $y0 += self::STEP) {
                $x = $x0;
                $y = $y0;
                for ($i = 0; $i <= $iterations; $i++) {
                    [$x, $y] = $this->iterate($x, $y);
                    if (is_nan($x) || is_nan($y) || is_infinite($x) || is_infinite($y)) {
                        break;
                    }
                }
                if (is_nan($x) || is_nan($y) || is_infinite($x) || is_infinite($y)) {
                    continue;
                }

                $this->xCoordinates .= $x0 . ',';
                $this->yCoordinates .= $y0 . ',';
            }
        }

        $this->xCoordinates = rtrim($this->xCoordinates, ',');
        $this->yCoordinates = rtrim($this->yCoordinates, ',');
    }

    public function iterate(float $x, float $y): array
    {
        $xPlus1 = $this->a0 + $this->a1*$x + $this->a2*$x**2 + $this->a3*$x*$y + $this->a4*$y + $this->a5*$y**2;
        $yPlus1 = $this->b0 + $this->b1*$x + $this->b2*$x**2 + $this->b3*$x*$y + $this->b4*$y + $this->b5*$y**2;
        return [$xPlus1, $yPlus1];
    }

    public function computeJacobian(float $x, float $y): Matrix
    {
        return MatrixFactory::create([
            [$this->a1 + 2*$this->a2 + $this->a3*$y, $this->a4 + 2*$this->a5 + $this->a3*$x],
            [$this->b1 + 2*$this->b2 + $this->b3*$y, $this->b4 + 2*$this->b5 + $this->b3*$x],
        ]);
    }

    public function calculateLyapunovExponentIteration(float $x, float $y): void
    {
        $jacobian = $this->computeJacobian($x, $y);
        // From doc: $AB   = $A->multiply($B);
        // Jk* = J(f^(k-1)(x))Q_(k-1)
        $jacobian = $jacobian->multiply($this->Q->transpose());
        $QR = $jacobian->qrDecomposition();
        // Save Q for next iteration
        $this->Q = $QR->Q;
        /**
         * As R is upper triangular we can just pull the diagonal coefficients immediately
         * We don't follow the procedure exactly of multiplying the R_ii values together but
         * by adding the Re(ln(R_ii)) as ln(x*y) = ln(x) + ln(y) as this keeps us within the
         * bounds of representable float values in PHP (64bit IEEE754 double precision)
         * Moreover as Re(ln(-x)) = ln(x) and Re(ln(x)) = ln(x) [for x > 0]
         * Thus ln(abs(x))
         */
        $this->diag1 += log(abs($QR->R[0][0]));
        $this->diag2 += log(abs($QR->R[1][1]));
    }

    public function computeLyapunovExponentsFromDiagonal(int $iterations): void
    {
        $this->lyapunov1 = (1/$iterations) * $this->diag1;
        $this->lyapunov2 = (1/$iterations) * $this->diag2;
    }

    public function getMathMLRepresentation(): string
    {
        $mathML = '<math>';
        $mathML .= '<mrow>';
        $mathML .= '<msub><mi>x</mi><mrow><mi>n</mi><mo>+</mo><mn>1</mn></mrow></msub>';
        $mathML .= '<mo>=</mo>';
        $mathML .= '<mn>' . $this->a0 . '</mn>';
        $mathML .= '<mo>+</mo><mn>' . $this->a1. '</mn><mi>x</mi>';
        $mathML .= '<mo>+</mo><mn>' . $this->a2. '</mn><msup><mi>x</mi><mn>2</mn></msup>';
        $mathML .= '<mo>+</mo><mn>' . $this->a3. '</mn><mi>x</mi><mi>y</mi>';
        $mathML .= '<mo>+</mo><mn>' . $this->a4. '</mn><mi>y</mi>';
        $mathML .= '<mo>+</mo><mn>' . $this->a5. '</mn><msup><mi>y</mi><mn>2</mn></msup>';
        $mathML .= '</mrow></math>';
        $mathML .= '<br>';
        $mathML .= '<math><mrow>';
        $mathML .= '<msub><mi>y</mi><mrow><mi>n</mi><mo>+</mo><mn>1</mn></mrow></msub>';
        $mathML .= '<mo>=</mo>';
        $mathML .= '<mn>' . $this->b0 . '</mn>';
        $mathML .= '<mo>+</mo><mn>' . $this->b1. '</mn><mi>x</mi>';
        $mathML .= '<mo>+</mo><mn>' . $this->b2. '</mn><msup><mi>x</mi><mn>2</mn></msup>';
        $mathML .= '<mo>+</mo><mn>' . $this->b3. '</mn><mi>x</mi><mi>y</mi>';
        $mathML .= '<mo>+</mo><mn>' . $this->b4. '</mn><mi>y</mi>';
        $mathML .= '<mo>+</mo><mn>' . $this->b5. '</mn><msup><mi>y</mi><mn>2</mn></msup>';
        $mathML .= '</mrow>';
        $mathML .= '</math>';
        return $mathML;
    }

    public function getLyapunovExponents(): array
    {
        return [$this->lyapunov1, $this->lyapunov2, $this->lyapunov1+$this->lyapunov2];
    }

    public function getXCoordinates(): string
    {
        return $this->xCoordinates;
    }

    public function getYCoordinates(): string
    {
        return $this->yCoordinates;
    }

    public function getLyapunovIteration(): string
    {
        return $this->lyapunovIteration;
    }

    public function getLyapunov1Coordinates(): string
    {
        return $this->lyapunov1Coordinates;
    }

    public function getLyapunov2Coordinates(): string
    {
        return $this->lyapunov2Coordinates;
    }
}
