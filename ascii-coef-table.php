<?php

/**
 * first coefficiant correspond to ASCII code 32 from Table 2.1,
 * (page 28-30)
 * of Strange Attractors: Creating Patterns in Chaos by Julien C. Sprott
 */

const ASCII_CONTROL_CHAR_SHIFTED_TABLE = [
    -4.5,
    -4.4,
    -4.3,
    -4.2,
    -4.1,
    -4.0,
    -3.9,
    -3.8,
    -3.7,
    -3.6,
    -3.5,
    -3.4,
    -3.3,
    -3.2,
    -3.1,
    -3.0,
    -2.9,
    -2.8,
    -2.7,
    -2.6,
    -2.5,
    -2.4,
    -2.3,
    -2.2,
    -2.1,
    -2.0,
    -1.9,
    -1.8,
    -1.7,
    -1.6,
    -1.5,
    -1.4,
    -1.3,
    -1.2,
    -1.1,
    -1.0,
    -0.9,
    -0.8,
    -0.7,
    -0.6,
    -0.5,
    -0.4,
    -0.3,
    -0.2,
    -0.1,
    0.0,
    0.1,
    0.2,
    0.3,
    0.4,
    0.5,
    0.6,
    0.7,
    0.8,
    0.9,
    1.0,
    1.1,
    1.2,
    1.3,
    1.4,
    1.5,
    1.6,
    1.7,
    1.8,
    1.9,
    2.0,
    2.1,
    2.2,
    2.3,
    2.4,
    2.5,
    2.6,
    2.7,
    2.8,
    2.9,
    3.0,
    3.1,
    3.2,
    3.3,
    3.4,
    3.5,
    3.6,
    3.7,
    3.8,
    3.9,
    4,
    4.1,
    4.2,
    4.3,
    4.4,
    4.5,
    4.6,
    4.7,
    4.8,
    4.9,
    5,
];

/* Needed to recover weird 16bit floating point handling from BASIC */
const EPSILON = 0.000000001;

function charToCoef(string $asciiCharacter): float
{
    return ASCII_CONTROL_CHAR_SHIFTED_TABLE[ord($asciiCharacter) - 32] /*- EPSILON*/;
}

function charCoefHtmlTable(): string
{
    $table = '<table><tbody>';
    $table .= '<thead><tr><th>Character</th><th>ASCII code</th><th>Coefficient value</th></tr></thead>';
    foreach (ASCII_CONTROL_CHAR_SHIFTED_TABLE as $key => $coef) {
        $char = chr($key + 32);
        $table .= "<tr><td>$char</td><td>" . ($key + 32) . "</td><td>$coef</td></tr>";
    }
    $table .= '</tbody></table>';
    return $table;
}