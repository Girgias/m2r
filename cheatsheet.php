<?php
require_once 'ascii-coef-table.php';

const LOGISTIC_MAP_CODE = 'AMu%';

//$mapCode = 'DFBIEVV';

// Index is equal to page number
const SPROTT_MAPS = [
    52 => ['map' => 'EWM?MPMMWMMMM', 'comment' => 'Hénon map'],
    58 => ['map' => 'EAGHNFODVNJCP', 'comment' => ''],
    59 => ['map' => 'EBCQAFMFVPXKQ', 'comment' => ''],
    60 => ['map' => 'EDSYUECINGQNV', 'comment' => ''],
    61 => ['map' => 'EELXAPXMPQOBT', 'comment' => ''],
    62 => ['map' => 'EEYYMKTUMXUVC', 'comment' => ''],
    63 => ['map' => 'EJTTSMBOGLLQF', 'comment' => ''],
    64 => ['map' => 'ENNMJRCTVVTYG', 'comment' => '7 cluster'],
    65 => ['map' => 'EOUGFJKDHSAJU', 'comment' => ''],
    66 => ['map' => 'EQKOCSIDVTPGY', 'comment' => 'Marcus\'s choice'],
    67 => ['map' => 'EQLOIARXYGHAJ', 'comment' => ''],
    68 => ['map' => 'ETJUBWEDNRORR', 'comment' => ''],
    69 => ['map' => 'ETSILUNDQSIFA', 'comment' => 'Rares\'s choice'],
    70 => ['map' => 'EUEBJLCDISIIQ', 'comment' => ''],
    71 => ['map' => 'EVDUOTLRBKTJD', 'comment' => ''],
    72 => ['map' => 'EWLKWPSMOGIGS', 'comment' => ''],
    73 => ['map' => 'EZPMSGCNFRENG', 'comment' => ''],
];

function examplePlotCodes(): string
{
    $table = '<table><tbody>';
    $table .= '<thead><tr><th>Page</th><th>Map code</th><th>Comment</th></tr></thead>';
    foreach (SPROTT_MAPS as $page => $data) {
        $table .= "<tr><td>$page</td><td>{$data['map']}</td><td>{$data['comment']}</td></tr>";
    }
    $table .= '</tbody></table>';
    return $table;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Discrete dynamical system plot generator - M2R 2020</title>
    <!-- Plotly.js -->
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>
<body>
<h1>Cheatsheet for Discrete dynamical system plot generator</h1>
<nav>
    <ul>
        <li><a href="/">Plot discrete dynamical system</a></li>
        <li><a href="cheatsheet.php">Cheatsheet</a></li>
    </ul>
</nav>
<p>The codes follows the coding from the book "Strange Attractors: Creating Patterns in Chaos" by Julien C. Sprott</p>
<p>
    The first letter represent the type of map.<br>
    A, B, C, and D are one dimensional maps of order 2, 3, 4, and 5 respectively.<br>
    E is a two dimensional quadratic map.
</p>
<p>The following characters represent a coefficient which is describe below.</p>
<p>Some examples taken from Sprott's book:</p>
<?= examplePlotCodes() ?>
<p>The following table is a correspondence of what character corresponds to what coefficient:</p>
<?= charCoefHtmlTable() ?>
</body>
</html>
