<?php

/** META TODO LIST
 * Handle infinite values in plot as JS chokes on value 'INF'
 * Handle NAN values (too small number division?)
 */

require_once './vendor/autoload.php';
require_once 'ascii-coef-table.php';
require_once 'OneDimensionalMap.php';
require_once 'TwoDimensionalQuadraticMap.php';
require_once 'mapcode-to-mapobject.php';

const PLOT_MARKER_SIZE = 2;
const PLOT_SIZE = '800px';
const BOUNDARIES_X = 3;
const BOUNDARY_Y_NEG = -8;
const BOUNDARY_Y_POS = 35;

/* Map setup */
$hasMap = false;
/* Sprott map code */
$mapCode = $_GET['code'] ?? '';
/* arbitrary coefficients */
$a0 = $_GET['a0'] ?? 0;
$a1 = $_GET['a1'] ?? 0;
$a2 = $_GET['a2'] ?? 0;
$a3 = $_GET['a3'] ?? 0;
$a4 = $_GET['a4'] ?? 0;
$a5 = $_GET['a5'] ?? 0;
$b0 = $_GET['b0'] ?? 0;
$b1 = $_GET['b1'] ?? 0;
$b2 = $_GET['b2'] ?? 0;
$b3 = $_GET['b3'] ?? 0;
$b4 = $_GET['b4'] ?? 0;
$b5 = $_GET['b5'] ?? 0;
/* Initial values */
$x = $_GET['x'] ?? 0.05;
$y = $_GET['y'] ?? 0.05;
$iterations = $_GET['iterations'] ?? 5000;
$transient = $_GET['transient'] ?? TwoDimensionalQuadraticMap::TRANSIENT_ITERATIONS;
/* Compute Lyapunov exponents? */
$hasLyapunovExponentCalculation = array_key_exists('calculate_lyapunov_exponents', $_GET);
/* Estimate basin of attraction? */
$hasBasin = array_key_exists('basin', $_GET);
$xNeg = $_GET['xNeg'] ?? -5;
$xPos = $_GET['xPos'] ?? 5;
$yNeg = $_GET['yNeg'] ?? -5;
$yPos = $_GET['yPos'] ?? 5;
// Plot versus fifth previous iterate
// Map x_n to x_(n+j)
$versusPredecessorN = 5;

function getPlotShareableLink(): string {
    global $mapCode, $a0, $a1, $a2, $a3, $a4, $a5, $b0, $b1, $b2, $b3, $b4, $b5, $x, $y;
    global $hasLyapunovExponentCalculation, $hasBasin, $transient, $iterations;
    $link = $_SERVER['HTTP_HOST'] . '/?';
    $link .= "transient=$transient&iterations=$iterations&";
    if ($mapCode !== '') {
        $link .= "code=$mapCode&";
    } else {
        if ($a0 !== 0) { $link .= "a0=$a0&"; }
        if ($a1 !== 0) { $link .= "a1=$a1&"; }
        if ($a2 !== 0) { $link .= "a2=$a2&"; }
        if ($a3 !== 0) { $link .= "a3=$a3&"; }
        if ($a4 !== 0) { $link .= "a4=$a4&"; }
        if ($a5 !== 0) { $link .= "a5=$a5&"; }
        if ($b0 !== 0) { $link .= "b0=$b0&"; }
        if ($b1 !== 0) { $link .= "b1=$b1&"; }
        if ($b2 !== 0) { $link .= "b2=$b2&"; }
        if ($b3 !== 0) { $link .= "b3=$b3&"; }
        if ($b4 !== 0) { $link .= "b4=$b4&"; }
        if ($b5 !== 0) { $link .= "b5=$b5&"; }
    }
    /* Initial values */
    $link .= "x=$x&y=$y&";
    if ($hasLyapunovExponentCalculation) {
        $link .= "calculate_lyapunov_exponents=on&";
    }
    if ($hasBasin) {
        $link .= "basin=on&";
    }
    return $link;
}

if ($mapCode !== '') {
    switch ($mapCode[0]) {
        case 'A':
        case 'B':
        case 'C':
        case 'D':
            $hasMap = true;
            break;
        case 'E':
            $map = fromTwoDimensionalCodeToMap($mapCode);
            $hasMap = true;
            break;
        default:
            throw new RuntimeException('Unknown code');
    }
} else if ((
        $a0 === 0 && $a1 === 0 && $a2 === 0 && $a3 === 0 && $a4 === 0 && $a5 === 0 &&
        $b0 === 0 && $b1 === 0 && $b2 === 0 && $b3 === 0 && $b4 === 0 && $b5 === 0
    ) === false) {
    $hasMap = true;
    $map = new TwoDimensionalQuadraticMap($a0, $a1, $a2, $a3, $a4, $a5, $b0, $b1, $b2, $b3, $b4, $b5,);
}

/** Generate the plot data */
if ($hasMap) {
    if ($map instanceof OneDimensionalMap) {
        $dimension = 1;
        $map = fromOneDimensionalCodeToMap($mapCode);
        $startIterateExecutionTime = hrtime(true);
        $map->generateCoordinates($iterations, $x);
        $iterationExecutionNanoseconds = hrtime(true) - $startIterateExecutionTime;
        /** Map x_N versus X_N+$versusPredecessorN */
        $map->mapToPreviousNthPoint($versusPredecessorN);
    } elseif ($map instanceof TwoDimensionalQuadraticMap) {
        $dimension = 2;
        $startIterateExecutionTime = hrtime(true);
        if ($hasBasin) {
            // Use a sane value
            if ($iterations > 1000) {
                $iterations = 1000;
            }
            $hasLyapunovExponentCalculation = false;
            $map->findBasinAttraction($iterations, $xNeg, $xPos, $yNeg, $yPos);
        } else if ($hasLyapunovExponentCalculation) {
            $map->generateCoordinatesWithLyapunovExponents($iterations, $transient, $x, $y);
        } else {
            $map->generateCoordinates($iterations, $transient, $x, $y);
        }
        $iterationExecutionNanoseconds = hrtime(true) - $startIterateExecutionTime;
    }

    $memoryUsage = memory_get_usage(false);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Discrete dynamical system plot generator - M2R 2020</title>
    <!-- Plotly.js -->
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>
<body>
    <h1>Discrete dynamical system plot generator</h1>
    <nav>
        <ul>
            <li><a href="/">Plot discrete dynamical system</a></li>
            <li><a href="/tent-map.php">Plot Tent map</a></li>
            <li><a href="/cheatsheet.php">Cheatsheet</a></li>
        </ul>
    </nav>
    <form action="" method="get" class="form-example">
        <fieldset>
            <legend>Equation set-up</legend>
            <p>
                If a code following Sprott's book convention is used it's this map
                that will be plotted, otherwise the manual set up one will be plotted.
            </p>
            <label for="code">Dynamical system code:</label>
            <input type="text" name="code" id="code" value="<?= $mapCode ?>">
            <br>
            <fieldset>
                <legend>Manual setup</legend>
                <math>
                    <mrow>
                        <msub><mi>x</mi><mrow><mi>n</mi><mo>+</mo><mn>1</mn></mrow></msub>
                        <mo>=</mo>
                        <msub><mi>a</mi><mn>0</mn></msub><mo>+</mo>
                        <msub><mi>a</mi><mn>1</mn></msub><mi>x</mi><mo>+</mo>
                        <msub><mi>a</mi><mn>2</mn></msub><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo>
                        <msub><mi>a</mi><mn>3</mn></msub><mi>x</mi><mi>y</mi><mo>+</mo>
                        <msub><mi>a</mi><mn>4</mn></msub><mi>y</mi><mo>+</mo>
                        <msub><mi>a</mi><mn>5</mn></msub><msup><mi>y</mi><mn>2</mn></msup>
                    </mrow>
                </math>
                <br>
                <math>
                    <mrow>
                        <msub><mi>y</mi><mrow><mi>n</mi><mo>+</mo><mn>1</mn></mrow></msub>
                        <mo>=</mo>
                        <msub><mi>b</mi><mn>0</mn></msub><mo>+</mo>
                        <msub><mi>b</mi><mn>1</mn></msub><mi>x</mi><mo>+</mo>
                        <msub><mi>b</mi><mn>2</mn></msub><msup><mi>x</mi><mn>2</mn></msup><mo>+</mo>
                        <msub><mi>b</mi><mn>3</mn></msub><mi>x</mi><mi>y</mi><mo>+</mo>
                        <msub><mi>b</mi><mn>4</mn></msub><mi>y</mi><mo>+</mo>
                        <msub><mi>b</mi><mn>5</mn></msub><msup><mi>y</mi><mn>2</mn></msup>
                    </mrow>
                </math>
                <br>
                <label for="a0">a<sub>0</sub> value:</label>
                <input type="text" name="a0" id="a0" value="<?= $a0 ?>" required>
                <br>
                <label for="a1">a<sub>1</sub> value:</label>
                <input type="text" name="a1" id="a1" value="<?= $a1 ?>" required>
                <br>
                <label for="a2">a<sub>2</sub> value:</label>
                <input type="text" name="a2" id="a1" value="<?= $a2 ?>" required>
                <br>
                <label for="a3">a<sub>3</sub> value:</label>
                <input type="text" name="a3" id="a3" value="<?= $a3 ?>" required>
                <br>
                <label for="a4">a<sub>4</sub> value:</label>
                <input type="text" name="a4" id="a4" value="<?= $a4 ?>" required>
                <br>
                <label for="a5">a<sub>5</sub> value:</label>
                <input type="text" name="a5" id="a5" value="<?= $a5 ?>" required>
                <br>
                <label for="b0">b<sub>0</sub> value:</label>
                <input type="text" name="b0" id="b0" value="<?= $b0 ?>" required>
                <br>
                <label for="b1">b<sub>1</sub> value:</label>
                <input type="text" name="b1" id="b1" value="<?= $b1 ?>" required>
                <br>
                <label for="b2">b<sub>2</sub> value:</label>
                <input type="text" name="b2" id="b1" value="<?= $b2 ?>" required>
                <br>
                <label for="b3">b<sub>3</sub> value:</label>
                <input type="text" name="b3" id="b3" value="<?= $b3 ?>" required>
                <br>
                <label for="b4">b<sub>4</sub> value:</label>
                <input type="text" name="b4" id="b4" value="<?= $b4 ?>" required>
                <br>
                <label for="b5">b<sub>5</sub> value:</label>
                <input type="text" name="b5" id="b5" value="<?= $b5 ?>" required>
                <br>
            </fieldset>
        </fieldset>
        <fieldset>
            <legend>Initial values</legend>
            <label for="x">Initial X value:</label>
            <input type="text" name="x" id="x" value="<?= $x ?>" required>
            <br>
            <label for="y">Initial Y value:</label>
            <input type="text" name="y" id="y" value="<?= $y ?>" required>
            <br>
        </fieldset>
        <fieldset>
            <legend>Lyapunov Expoenent configuration</legend>
            <label for="calculate_lyapunov_exponents">Calculate Lyapunov exponents?</label>
            <input type="checkbox" name="calculate_lyapunov_exponents" id="calculate_lyapunov_exponents"
                <?php if ($hasLyapunovExponentCalculation) { echo "checked"; } ?>>
            <br>
        </fieldset>
        <fieldset>
            <legend>Plot generation</legend>
            <p>
                5000 iterations is a good base line<br>
                10 000 to 20 000 for non dense attractors<br>
                50 000 for dense attractors<br>
            </p>
            <label for="iterations">Number of iterations:</label>
            <input type="number" name="iterations" id="iterations" value="<?= $iterations ?>" required>
            <br>
            <label for="transient">Transient step (i.e. how many iterations to skip):</label>
            <input type="text" name="transient" id="transient" value="<?= $transient ?>" required>
            <br>
        </fieldset>
        <fieldset>
            <legend>Estimate basin of attraction</legend>
            <p>Toggle to switch from plot of dynamical system and  lyapunov exponent to basin of attraction</p>
            <label for="basin">Estimate basin of attraction?</label>
            <input type="checkbox" name="basin" id="basin" <?php if ($hasBasin) { echo "checked"; } ?>>
            <br>
            <label for="xNeg">Negative X boundary:</label>
            <input type="text" name="xNeg" id="xNeg" value="<?= $xNeg ?>" required>
            <br>
            <label for="xPos">Positive X boundary:</label>
            <input type="text" name="xPos" id="xPos" value="<?= $xPos ?>" required>
            <br>
            <label for="yNeg">Negative Y boundary:</label>
            <input type="text" name="yNeg" id="yNeg" value="<?= $yNeg ?>" required>
            <br>
            <label for="yPos">Positive Y boundary:</label>
            <input type="text" name="yPos" id="yPos" value="<?= $yPos ?>" required>
            <br>
        </fieldset>
        <input type="submit" value="Plot">
    </form>
    <?php if ($hasMap) { ?>
        <p>
            Points calculated using PHP <?= PHP_VERSION ?><br>
            Plot generated with <a href="https://plotly.com">Plotly</a> using its
            <a href="https://plotly.com/javascript/">plotly.js</a> library for JavaScript<br>
            Share this plot: <output><?= getPlotShareableLink() ?></output>
        </p>
        <?= $map->getMathMLRepresentation() ?>
    <p>
        Calculation of iterates:<br>
        <?= $iterations ?> iterations.<br>
        Executed in <?= $iterationExecutionNanoseconds ?> nanoseconds
        (<?= round($iterationExecutionNanoseconds * 1e-9, 3) ?> seconds).
        <br>
        Total Memory used: <?= round($memoryUsage / 1048576, 2) ?>MB.
    </p>
    <?php
    if (strpos($map->getYCoordinates(), 'INF') !== false) /* Needle inf is found */ {
        echo '<p>Plot is unbounded (value goes to infinity)</p>';
    }
    if (strpos($map->getYCoordinates(), 'NAN') !== false) /* Needle NAN is found */ {
        echo '<p>Plot has NAN (not a number) value</p>';
    }
    if ($dimension === 2 && $hasLyapunovExponentCalculation) {
        $lyapunovExponents = $map->getLyapunovExponents();
        echo '<p>Lyapunov exponents: ' . $lyapunovExponents[0] . ' and ' . $lyapunovExponents[1] .
            ' sum: ' . $lyapunovExponents[2] . '</p>';
    }
    ?>
    <!-- Plots go in blank <div> elements.
        You can size them in the plot layout,
        or give the div a size as shown here.
    -->
    <p>Plot of <?= $mapCode ?></p>
    <div id="iterated-map" style="width:<?= PLOT_SIZE ?>;height:<?= PLOT_SIZE ?>;"></div>
    <script>
        <!-- JS Code for Plotly -->
        let trace = {
            x: [ <?= $map->getXCoordinates() ?> ],
            y: [ <?= $map->getYCoordinates() ?> ],
            mode: 'markers',
            marker: {
                size: <?= PLOT_MARKER_SIZE ?>
            },
            type: 'scatter'
        }
        let data = [trace];
        Plotly.newPlot('iterated-map', data);
    </script>
        <?php if ($dimension === 2 && $hasLyapunovExponentCalculation) { ?>
        <p>Lyapunov exponent convergence for <?= $mapCode ?> after a transient step of <?= $transient ?></p>
        <div id="lyapunov-exponents" style="width:<?= PLOT_SIZE ?>;height:<?= PLOT_SIZE ?>;"></div>
        <script>
            let lyapunov1 = {
                x: [ <?= $map->getLyapunovIteration() ?> ],
                y: [ <?= $map->getLyapunov1Coordinates() ?> ],
                mode: 'markers',
                marker: {
                    size: <?= PLOT_MARKER_SIZE ?>
                },
                type: 'scatter',
                name: 'λ1'
            };
            let lyapunov2 = {
                x: [ <?= $map->getLyapunovIteration() ?> ],
                y: [ <?= $map->getLyapunov2Coordinates() ?> ],
                mode: 'markers',
                marker: {
                    size: <?= PLOT_MARKER_SIZE ?>
                },
                type: 'scatter',
                name: 'λ2'
            }
            let lyapunovExponentsConvergence = [lyapunov1, lyapunov2];
            Plotly.newPlot('lyapunov-exponents', lyapunovExponentsConvergence);
        </script>
        <?php } ?>
    <?php } ?>
</body>
</html>
