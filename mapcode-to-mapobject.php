<?php

require_once 'ascii-coef-table.php';
require_once 'OneDimensionalMap.php';
require_once 'TwoDimensionalQuadraticMap.php';

function fromOneDimensionalCodeToMap(string $code): OneDimensionalMap {
    $length = strlen($code);
    switch ($code[0]) {
        case 'A': assert($length === 4); break;
        case 'B': assert($length === 5); break;
        case 'C': assert($length === 6); break;
        case 'D': assert($length === 7); break;
        default:
            throw new RuntimeException('Unknown code order for one dimensional maps');
    }
    $coefficients = [];
    for ($i = 1; $i < $length; $i++) {
        $coefficients[] = charToCoef($code[$i]);
    }
    return new OneDimensionalMap(...$coefficients);
}

function fromTwoDimensionalCodeToMap(string $code): TwoDimensionalQuadraticMap {
    $length = strlen($code);
    switch ($code[0]) {
        case 'E': assert($length === 13); break;
        default:
            throw new RuntimeException('Unknown code order for two dimensional maps');
    }
    $coefficients = [];
    for ($i = 1; $i < $length; $i++) {
        $coefficients[] = charToCoef($code[$i]);
    }
    return new TwoDimensionalQuadraticMap(...$coefficients);
}
