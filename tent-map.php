<?php

/** META TODO LIST
 * Handle infinite values in plot as JS chokes on value 'INF'
 * Handle NAN values (too small number division?)
 */

require_once './vendor/autoload.php';
require_once 'OneDimensionalMap.php';
require_once 'TentMap.php';

const PLOT_MARKER_SIZE = 4;
const PLOT_SIZE = '800px';

/* Map setup */
$hasMap = false;
/* Mu value */
$mu = $_GET['mu'] ?? 0;
/* Initial values */
$x = $_GET['x'] ?? 0.05;

$iterations = $_GET['iterations'] ?? 5000;
$versus = $_GET['versus'] ?? 1;
/*
$transient = $_GET['transient'] ?? TwoDimensionalQuadraticMap::TRANSIENT_ITERATIONS;
// Compute Lyapunov exponents?
$hasLyapunovExponentCalculation = array_key_exists('calculate_lyapunov_exponents', $_GET);
// Plot versus fifth previous iterate
// Map x_n to x_(n+j)
$versusPredecessorN = 5;
*/

function getPlotShareableLink(): string {
    global $mu, $versus, $x, $y;
    global $hasLyapunovExponentCalculation, $transient, $iterations;
    $link = $_SERVER['HTTP_HOST'] . '/tent-map.php?';
    $link .= "mu=$mu&";
    /* Initial values */
    $link .= "x=$x&y=$y&";
    $link .= "versus=$versus&";
    $link .= "iterations=$iterations";
    return $link;
}

if ($mu !== 0) {
    $hasMap = true;
}

/** Generate the plot data */
if ($hasMap) {
        $map = new TentMap($mu);
        $startIterateExecutionTime = hrtime(true);
        $map->generateCoordinates($iterations, $x);
        $iterationExecutionNanoseconds = hrtime(true) - $startIterateExecutionTime;
        /** Map x_N versus X_N+$versusPredecessorN */
        $map->mapToPreviousNthPoint($versus);

    $memoryUsage = memory_get_usage(false);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Discrete dynamical system plot generator - M2R 2020</title>
    <!-- Plotly.js -->
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>
<body>
    <h1>Tent map plot generator</h1>
    <nav>
        <ul>
            <li><a href="/">Plot discrete dynamical system</a></li>
            <li><a href="/tent-map.php">Plot Tent map</a></li>
            <li><a href="/cheatsheet.php">Cheatsheet</a></li>
        </ul>
    </nav>
    <form action="" method="get" class="form-example">
        <fieldset>
            <legend>Equation set-up</legend>
            <label for="mu">Mu:</label>
            <input type="text" name="mu" id="mu" value="<?= $mu ?>">
            <br>
        </fieldset>
        <fieldset>
            <legend>Initial values</legend>
            <label for="x">Initial X value:</label>
            <input type="text" name="x" id="x" value="<?= $x ?>" required>
            <br>
        </fieldset>
        <!--
        <fieldset>
            <legend>Lyapunov Expoenent configuration</legend>
            <label for="calculate_lyapunov_exponents">Calculate Lyapunov exponents?</label>
            <input type="checkbox" name="calculate_lyapunov_exponents" id="calculate_lyapunov_exponents" checked>
            <br>
            <label for="transient">Transient step (i.e. how many iterations to skip before starting to compute):</label>
            <input type="text" name="transient" id="transient" value="<?= $transient ?>" required>
            <br>
        </fieldset>
        -->
        <fieldset>
            <legend>Plot generation</legend>
            <p>
                5000 iterations is a good base line<br>
                10 000 to 20 000 for non dense attractors<br>
                50 000 for dense attractors<br>
            </p>
            <label for="iterations">Number of iterations:</label>
            <input type="number" name="iterations" id="iterations" value="<?= $iterations ?>" required>
            <br>
            <label for="versus">Y = X_(n+?)</label>
            <input type="number" name="versus" id="versus" value="<?= $versus ?>" required>
        </fieldset>
        <input type="submit" value="Plot">
    </form>
    <?php if ($hasMap) { ?>
        <p>
            Points calculated using PHP <?= PHP_VERSION ?><br>
            Plot generated with <a href="https://plotly.com">Plotly</a> using its
            <a href="https://plotly.com/javascript/">plotly.js</a> library for JavaScript<br>
            Share this plot: <output><?= getPlotShareableLink() ?></output>
        </p>
        <?= $map->getMathMLRepresentation() ?>
    <p>
        Calculation of iterates:<br>
        <?= $iterations ?> iterations.<br>
        Executed in <?= $iterationExecutionNanoseconds ?> nanoseconds
        (<?= round($iterationExecutionNanoseconds * 1e-9, 3) ?> seconds).
        <br>
        Total Memory used: <?= round($memoryUsage / 1048576, 2) ?>MB.
    </p>
    <?php
    if (strpos($map->getYCoordinates(), 'INF') !== false) /* Needle inf is found */ {
        echo '<p>Plot is unbounded (value goes to infinity)</p>';
    }
    if (strpos($map->getYCoordinates(), 'NAN') !== false) /* Needle NAN is found */ {
        echo '<p>Plot has NAN (not a number) value</p>';
    }
    ?>
    <!-- Plots go in blank <div> elements.
        You can size them in the plot layout,
        or give the div a size as shown here.
    -->
    <p>Plot of tent map with mu=<?= $mu ?></p>
    <div id="iterated-map" style="width:<?= PLOT_SIZE ?>;height:<?= PLOT_SIZE ?>;"></div>
    <script>
        <!-- JS Code for Plotly -->
        let trace = {
            x: [ <?= $map->getXCoordinates() ?> ],
            y: [ <?= $map->getYCoordinates() ?> ],
            mode: 'markers',
            marker: {
                size: <?= PLOT_MARKER_SIZE ?>
            },
            type: 'scatter'
        }
        let xyLine = {
            x: [0, 1],
            y: [0, 1],
            mode: 'line',
            type: 'scatter'
        }
        let data = [trace, xyLine];
        Plotly.newPlot('iterated-map', data);
    </script>
    <?php } ?>
</body>
</html>
